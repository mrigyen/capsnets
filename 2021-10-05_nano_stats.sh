# mnist data
guild run mnist.py USE_GPU=True --label "Nano MNIST CPU"
guild run mnist.py USE_GPU=True --label "Nano MNIST GPU"
# emnist data
guild run emnist.py USE_GPU=True --label "Nano EMNIST CPU"
guild run emnist.py USE_GPU=False --label "Nano EMNIST GPU"
# cifar data
guild run cifar10.py USE_GPU=True --label "Nano CIFAR10 CPU"
guild run cifar10.py USE_GPU=False --label "Nano CIFAR10 GPU"
# svhn data
guild run svhn.py USE_GPU=True --label "Nano SVHN CPU"
guild run svhn.py USE_GPU=False --label "Nano SVHN GPU"
