from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
import log
import decomposition
import time
from torch.profiler import profile, record_function, ProfilerActivity
import prof_helper

def squash(x, epsilon=1e-8):
    """
    The squash function, exactly as defined for capsule networks by Hinton

    Squashes all the activation vectors, assuming the last dimension is the
    activation vector size.
    """
    assert not torch.isnan(x).any()
    # log.debug(f"squash() x.shape = {x.shape}")

    # return sum of each row of squared x in the last dimension and keep
    # the same shape except the last dimension
    # keepdim=True ensures that the output is tensor([num]) and not tensor(num)
    # if you don't use epsilon term, you'll get an undefined gradient due to
    # division by zero when torch.sqrt(squared_norm) equals zero
    # see issue: https://github.com/jindongwang/Pytorch-CapsuleNet/issues/7
    squared_norm = ((x ** 2) + epsilon).sum(-1, keepdim=True)
    # log.debug(f"squash() squared_norm.shape = {squared_norm.shape}")

    output = (squared_norm / (1.0 + squared_norm)) * (x / torch.sqrt(squared_norm))
    # log.debug(f"squash() output.shape = {output.shape}")
    return output

class ConvLayer(nn.Module):
    def __init__(self,
            in_channels=1, # number of channels in input image
            out_channels=256, # number of channels produced by the convolution
            kernel_size=9, # square kernel
            ):
        # without this line python throws an error when you attempt to init
        # TODO figure out why this line is necessary
        super(ConvLayer, self).__init__()
        """
        Wrapper for the convolutional layers used in capsnets

        Values of in_channels, out_channels, and kernel_size is equal to that
        in the 2017 paper
        """
        self.conv = nn.Conv2d(in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size)
        self.relu = nn.ReLU()

    def forward(self, x):
        assert not torch.isnan(x).any()
        # verify input dimensions for self.conv()
        # ref for shape = https://stackoverflow.com/questions/47128044/
        assert len(x.shape) == 4, "Expected input of shape  (Batch, Number Channels, height, width)"
        u = self.conv(x)
        u = self.relu(u)
        # log.debug(f"ConvLayer.forward() u.shape = {u.shape}")
        # haven't added an assert because it is a bit complicated to do so
        # ConvLayer.forward() u.shape = torch.Size([1, 256, 20, 20])
        return u

    def test(self, x=torch.rand(1, 1, 28, 28)):
        """
        Black box test the class and print results
        The input argument x has a default corresponding to the shape Conv2d
        requires, and according to the paper's use of the layer.
        """
        log.debug("Testing ConvLayer")
        log.debug(f"x.shape = {x.shape}")
        u = self.forward(x)
        log.debug(f"u.shape = {u.shape}")
        return u

    def num_params(self):
        return sum([p.numel() for p in self.conv.parameters()])


class PrimaryCapsLayer(nn.Module):
    def __init__(self,
            # based on u[0].shape
            num_routes=32 * 6 * 6, # number of capsules / activation vectors we end up with
            num_non_capsules=8,
            # each capsule consists of Conv2d networks using the following values
            in_channels=256, # input to every non capsule is torch.Size([1, 256, 20, 20])
            out_channels=32, # the output of every non capsule contains 32 channels
            kernel_size=9,
            stride=2):
        """
        Intermediate layer of capsules
        Default values of arguments are based on the 2017 Hinton paper

        Non-capsules are independent convolutional layers, whose individual
        neurons of the same indices, when collected together, constitute a
        virtual capsule.
        Therefore, num_non_capsules equals the size of the activation vector
        from each of the capsules in this layer.
        """
        # TODO figure out why I need to add this to prevent the error
        # error: AttributeError: cannot assign module before Module.__init__() call
        super(PrimaryCapsLayer, self).__init__()
        self.num_routes = num_routes
        self.num_non_capsules = num_non_capsules
        # create a list of capsules of length num_non_capsules
        self.non_capsules = nn.ModuleList([
            nn.Conv2d(in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride)
            for _ in range(num_non_capsules)])

    def forward(self, x):
        assert not torch.isnan(x).any()
        # x.shape = torch.Size([1, 256, 20, 20])
        # log.debug(f"PrimaryCapsLayer.forward() x.shape = {x.shape}")

        # send the input in parallel to all non-capsules
        # technically we create the activation vector for the virtual capsules
        # by combining the array[i][j] values of multiple values of this list
        u = [non_capsule(x) for non_capsule in self.non_capsules]
        # log.debug(f"PrimaryCapsLayer.forward() u[0].shape = {u[0].shape}")
        print(u[0].shape)
        # u[0].shape = torch.Size([1, 32, 6, 6])

        # concatenate a sequence of tensors into a tensor, with a specific dimension
        # specifically, what happens here is a form of transpose
        u = torch.stack(u, dim=1)
        # log.debug(f"PrimaryCapsLayer.forward() u.shape = {u.shape}")
        # u.shape = torch.Size([1, 8, 32, 6, 6])

        # reshape the tensor so we have batches of activation vectors with
        # shape = [batch, self.num_routes, size(activation_vector)]
        # or [batch, number_of_activation_vectors, size(activation_vector)]
        #
        # pytorch guesses the "-1" element for the shape based on the other values
        u = u.view(x.size(0), self.num_routes, -1)
        # log.debug(f"PrimaryCapsLayer.forward() u.shape = {u.shape}")
        # u.shape = torch.Size([1, 1152, 8])

        # squash all activation vectors individually
        u = squash(u)
        # log.debug(f"PrimaryCapsLayer.forward() u.shape = {u.shape}")
        # u.shape = torch.Size([1, 1152, 8])

        return u

    def tucker_decompose_layers(self):
        """
        Replace all conv layers with tucker decomposed layers
        """
        for i, conv_layer in enumerate(self.non_capsules):
            self.non_capsules[i] = decomposition.tucker_decomposition_conv_layer(conv_layer)

    def test(self, x=torch.rand(1, 256, 20, 20)):
        log.debug("Testing PrimaryCapsLayer")
        log.debug(f"x.shape = {x.shape}")
        u = self.forward(x)
        log.debug(f"u.shape = {u.shape}")
        self.tucker_decompose_layers()
        log.debug("Testing PrimaryCapsLayer after Tucker Decomposition")
        log.debug(f"x.shape = {x.shape}")
        u = self.forward(x)
        log.debug(f"u.shape = {u.shape}")
        return u

    def num_params(self):
        return sum([p.numel() for p in self.non_capsules.parameters()])


class FinalCapsLayer(nn.Module):
    def __init__(self,
            num_routes=32 * 6 * 6, # 1152
            num_capsules=10, # number of capsules in this layer
            # each capsule consists of Conv2d networks using the following values
            in_channels=8, # input consists of 8D vectors
            out_channels=16, # capsule return a 16D vector
            num_iterations=3, # number of iterations used to compute the coupling coefficients
            ):
        # TODO clarify the use of num_routes
        """
        Final capsule layer, used to classify the input image

        Here num_capsules is actually used properly: it actually corresponds to
        the number of capsules and their outputs of this layer.
        """
        super(FinalCapsLayer, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.num_routes = num_routes
        self.num_capsules = num_capsules
        self.num_iterations = num_iterations

        # weight matrix
        self.W = nn.Parameter(torch.randn(1, num_routes, num_capsules, out_channels, in_channels))
        # self.W.shape = torch.Size([1, 1152, 10, 16, 8])

    def forward(self, x):
        assert not torch.isnan(x).any()
        assert x.shape[1:] == torch.Size([self.num_routes, self.in_channels])
        # x.shape = torch.Size([1, 1152, 8])

        batch_size = x.size(0)

        # each input activation vector now has 10 copies of itself
        # this is required so every capsule in this layer has access to all the
        # input activation vectors
        u = torch.stack([x] * self.num_capsules, dim=2)
        # u.shape = [batch, activation vectors, copies, elements in each vector]
        # u.shape = torch.Size([1, 1152, 10, 8])

        # Wrap every scalar of each tensor into its own tensor
        # TODO why is this done?
        u = u.unsqueeze(4)
        # log.debug(f"FinalCapsLayer.forward() u.shape = {u.shape}")
        # u.shape = [batch, activation vectors, copies, number of solo vectors, an individual element]
        # u.shape = torch.Size([1, 1152, 10, 8, 1])

        # make copies of self.W for use with every batch
        W = torch.cat([self.W] * batch_size, dim=0)
        # log.debug(f"FinalCapsLayer.forward() W.shape = {W.shape}")
        # W.shape = torch.Size([batch, 1152, 10, 16, 8])
        # W.shape = torch.Size([1, 1152, 10, 16, 8])

        # calculate the prediction vectors
        # batched matrix multiplication where the final two dimensions are used
        # to do 2D matrix multiplication
        # W.shape = torch.Size([1, 1152, 10, 16, 8])
        # u.shape = torch.Size([1, 1152, 10, 8, 1])
        # u_hat.shape = torch.Size([1, 1152, 10, 16, 1])
        u_hat = torch.matmul(W, u)
        # log.debug(f"FinalCapsLayer.forward() u_hat.shape = {u_hat.shape}")
        # u_hat.shape = torch.Size([1, 1152, 10, 16, 1])

        # initialize log prior probabilities that capsule i and j should be
        # coupled
        # TODO verify use of Variable is correct
        # TODO check if any other thing can be used here instead
        b_ij = Variable(torch.zeros(1, self.num_routes, self.num_capsules, 1))
        # log.debug(f"FinalCapsLayer.forward() b_ij.shape = {b_ij.shape}")
        # b_ij.shape = torch.Size([1, 1152, 10, 1])

        # TODO see the CUDA section, implement if necessary

        # dynamic routing
        # iteratively refine the coupling coefficients
        for iteration in range(self.num_iterations):
            # compute coupling coefficients between capsule i and all capsules
            # in the previous layer
            # capsules sum to 1 and are determined by a "routing softmax" on b_ij
            c_ij = F.softmax(b_ij, dim=1)
            # log.debug(f"FinalCapsLayer.forward() R1: c_ij.shape = {c_ij.shape}")
            # sum([c_ij[0][x][0] for x in range(c_ij.shape[1])]) == tensor([1.0000])
            # c_ij.shape = torch.Size([1, 1152, 10, 1])

            # clone coupling coefficients for each batch
            c_ij = torch.cat([c_ij] * batch_size, dim=0).unsqueeze(4)
            # log.debug(f"FinalCapsLayer.forward() R2: c_ij.shape = {c_ij.shape}")
            # c_ij.shape = torch.Size([1, 1152, 10, 1, 1])

            # compute the weighted sum over all prediction vectors from
            # previous layer capsules
            c_ij = c_ij.to(u_hat.device)
            s_j = (c_ij * u_hat).sum(dim=1, keepdim=True)
            # log.debug(f"FinalCapsLayer.forward() R3: s_j.shape = {s_j.shape}")
            # s_j.shape = torch.Size([1, 1, 10, 16, 1])

            # squash the input vectors to compute the new activation vectors of
            # this layer (not considered as the actual activation vector until
            # the final iteration)
            v_j = squash(s_j)
            # log.debug(f"FinalCapsLayer.forward() R4: v_j.shape = {v_j.shape}")
            # v_j.shape = torch.Size([1, 1, 10, 16, 1])

            # for all iterations except the last one
            # refining b_ij in the last iteration is wasteful since it isn't
            # used
            if iteration < self.num_iterations - 1:
                # compute the agreement between output of each capsule j and
                # prediction u_j|i made by capsule i
                a_ij = torch.matmul(u_hat.transpose(3, 4),
                        torch.cat([v_j] * self.num_routes, dim=1))
                # log.debug(f"FinalCapsLayer.forward() R5: a_ij.shape = {a_ij.shape}")
                # a_ij.shape = torch.Size([1, 1152, 10, 1, 1])

                # add this log likelihood to the initial logit b_ij
                # done before computing the new values for all the coupling
                # coefficients linking the previous layer to the current layer
                # log.info(f"b_ij.device = {b_ij.device}")
                # log.info(f"a_ij.device = {a_ij.device}")
                b_ij = b_ij.to(a_ij.device)
                b_ij = b_ij + a_ij.squeeze(4).mean(dim=0, keepdim=True)
                # log.debug(f"FinalCapsLayer.forward() R6: b_ij.shape = {b_ij.shape}")
                # b_ij.shape = torch.Size([1, 1152, 10, 1])

        # log.debug(f"FinalCapsLayer.forward() R8: v_j.shape = {v_j.shape}")
        # v_j.shape = torch.Size([1, 1, 10, 16, 1])

        # remove an unnecessary dimension
        out = v_j.squeeze(1)
        # return vector outputs of capsules of this layer
        # 10 activation vectors each of length 16
        # log.debug(f"R7: out.shape = {out.shape}")

        assert out.shape[1:] == torch.Size([self.num_capsules, self.out_channels, 1])
        # out.shape = torch.Size([1, 10, 16, 1])
        return out

    def test(self, x=torch.rand([1, 1152, 8])):
        # TODO replace prints with logs
        log.debug("Testing FinalCapsLayer")
        log.debug(f"x.shape = {x.shape}")
        u = self.forward(x)
        log.debug(f"u.shape = {u.shape}")
        # u.shape = torch.Size([1, 10, 16, 1])
        return u

    def num_params(self):
        return self.W.numel()


class Decoder(nn.Module):
    """
    Decoder network attached to the masked output of FinalCapsLayer to
    reconstruct the image
    """
    def __init__(self,
            input_channels=16,
            num_input_capsules=10,
            output_width=28,
            output_height=28,
            output_channels=1):
        super(Decoder, self).__init__()

        self.input_channels = input_channels
        self.num_input_capsules = num_input_capsules
        self.output_width = output_width
        self.output_height = output_height
        self.output_channels = output_channels
        self.reconstruction_layers = nn.Sequential(
                # self.input_channels * self.num_input_capsules is 160 by default
                nn.Linear(self.input_channels * self.num_input_capsules, 512),
                # TODO why inplace?
                nn.ReLU(inplace=True),
                nn.Linear(512, 1024),
                nn.ReLU(inplace=True),
                nn.Linear(1024, self.output_width * self.output_height * self.output_channels),
                nn.Sigmoid(),
                )
        self.SVD_decomposed = False

    def forward(self, x):
        assert not torch.isnan(x).any()
        assert x.shape[1:] == torch.Size([self.num_input_capsules, self.input_channels, 1])
        # x.shape = torch.Size([1, 10, 16, 1])
        # log.debug(f"D1: x.shape = {x.shape}")

        # get magnitude of final activation vectors
        classes = torch.sqrt((x ** 2).sum(2))
        # log.debug(f"D2: classes.shape = {classes.shape}")
        # classes.shape = torch.Size([1, 10, 1])

        # compute the softmax of the classes
        # sum of the value of classes of a specific batch now equals 1.0
        # also see https://github.com/jindongwang/Pytorch-CapsuleNet/issues/8
        classes = F.softmax(classes, dim=1)
        # log.debug(f"D3: classes.shape = {classes.shape}")
        # classes.shape = torch.Size([1, 10, 1])

        # get the indices of the max magnitude capsule for all batches
        _, max_magnitude_indices = classes.max(dim=1)
        # log.debug(f"D4: max_magnitude_indices.shape = {max_magnitude_indices.shape}")
        # max_magnitude_indices.shape = torch.Size([1, 1])

        # create an identity matrix of 10x10
        # masked = Variable(torch.sparse.torch.eye(10))
        masked = Variable(torch.sparse.torch.eye(self.num_input_capsules))
        # log.debug(f"D5: masked.shape = {masked.shape}")
        # masked.shape = torch.Size([10, 10])
        # TODO add CUDA if necessary

        # create a matrix of shape batch x 10, where each row represents the 10
        # classes of the batch and all indices equal 0 except the max magnitude
        # index, which equals 1
        # log.info(f"masked.device = {masked.device}")
        # log.info(f"max_magnitude_indices.device = {max_magnitude_indices.device}")
        masked = masked.to(max_magnitude_indices.device)
        masked = masked.index_select(dim=0,
                index=Variable(max_magnitude_indices.squeeze(1).data))
        # log.debug(f"D6: masked.shape = {masked.shape}")
        # masked.shape = torch.Size([batch, 10])

        # reshape so you can multiply it with x
        reshaped_masked = masked[:, :, None, None]
        # reshaped_masked.shape = torch.Size([batch, 10, 1, 1])

        # return a weighted masked tensor: that is, the vector of only the
        # "correct" class is left alone while every other vector is zeroed out
        weighted_masked_input = x * reshaped_masked
        # log.debug(f"x = {x}")
        # log.debug(f"reshaped_masked = {reshaped_masked}")
        # log.debug(f"weighted_masked_input = {weighted_masked_input}")
        # weighted_masked_input.shape = torch.Size([batch, 10, 16, 1])

        # Unroll the weighted activation vectors and therefore prepare the
        # input to be piped into the decoder network
        t = weighted_masked_input.view(x.size(0), -1)
        # log.debug(f"D6: t.shape = {t.shape}")
        # t.shape = torch.Size([batch, 160])
        assert t.shape[1] == self.input_channels * self.num_input_capsules

        # pipe it through the decoder network
        reconstructions = self.reconstruction_layers(t)
        # log.debug(f"D7: reconstructions.shape = {reconstructions.shape}")
        # reconstructions.shape = torch.Size([batch, 784])

        # get the reconstructed image
        # TODO maybe this should be widthXheight?
        reconstructions = reconstructions.view(-1,
                self.output_channels,
                self.output_height,
                self.output_width)
        # log.debug(f"D8: reconstructions.shape = {reconstructions.shape}")
        # reconstructions.shape = torch.Size([1, 1, 28, 28])

        assert reconstructions.shape[1:] == torch.Size([
            self.output_channels, self.output_height, self.output_width])
        assert masked.shape[1:] == torch.Size([self.num_input_capsules])
        return reconstructions, masked

    def test(self, x=torch.rand([1, 10, 16, 1])):
        # TODO replace prints with logs
        log.debug("Testing Decoder")
        log.debug(f"DX: x.shape = {x.shape}")
        u, masked = self.forward(x)
        log.debug(f"DY: u.shape = {u.shape}")
        log.debug(f"DY: masked.shape = {masked.shape}")
        # u.shape = torch.Size([1, 1, 28, 28])
        # masked.shape = torch.Size([1, 10])
        return u, masked

    def svd_decompose_layers(self):
        """
        Replace all nn.Linear layers with SVD decomposed layers
        """
        if self.SVD_decomposed == False:
            assert len(self.reconstruction_layers) == 6
            linear_layer_one = self.reconstruction_layers[0]
            relu_one = self.reconstruction_layers[1]
            linear_layer_two = self.reconstruction_layers[2]
            relu_two = self.reconstruction_layers[3]
            linear_layer_three = self.reconstruction_layers[4]
            sigmoid = self.reconstruction_layers[5]

            svd_layer_one = decomposition.svd_decomposition_fnn_layer(linear_layer_one)
            svd_layer_two = decomposition.svd_decomposition_fnn_layer(linear_layer_two)
            svd_layer_three = decomposition.svd_decomposition_fnn_layer(linear_layer_three)
            self.reconstruction_layers = nn.Sequential(
                    svd_layer_one[0],
                    svd_layer_one[1],
                    relu_one,
                    svd_layer_two[0],
                    svd_layer_two[1],
                    relu_two,
                    svd_layer_three[0],
                    svd_layer_three[1],
                    sigmoid,
                    )
            self.SVD_decomposed = True

    def num_params(self):
        return sum([p.numel() for p in self.reconstruction_layers.parameters()])


class CapsNet(nn.Module):
    """
    ConvLayer + PrimaryCapsLayer -> FinalCapsLayer
    """
    def __init__(self,
            conv_layer=ConvLayer(),
            primary_caps_layer=PrimaryCapsLayer(),
            final_caps_layer=FinalCapsLayer(),
            decoder_network=Decoder(),
            num_classes=10,
            index_one=False, # whether labels in dataset uses one indexing
            ):
        super(CapsNet, self).__init__()

        self.conv_layer = conv_layer
        self.primary_caps_layer = primary_caps_layer
        self.final_caps_layer = final_caps_layer
        self.decoder_network = decoder_network
        self.mse_loss = nn.MSELoss()
        self.num_classes = num_classes
        self.index_one = index_one

    def forward(self, input_to_capsnet):
        """
        I don't think there's a need of asserts here since CapsNet is a wrapper
        over more fundamental classes that use asserts
        """
        assert not torch.isnan(input_to_capsnet).any()
        capsnet_output = self.final_caps_layer(
                self.primary_caps_layer(
                    self.conv_layer(input_to_capsnet)))
        reconstructions, masked = self.decoder_network(capsnet_output)
        return capsnet_output, reconstructions, masked

    def margin_loss(self,
            capsnet_output,
            labels,
            m_plus=0.9,
            m_minus=0.1,
            lambda_val=0.5,
            ):
        """
        TODO Verify this margin loss is as defined in the paper
        TODO What is the type of labels
        """
        assert not torch.isnan(capsnet_output).any()
        assert not torch.isnan(labels).any()
        # TODO check logs for the value of this
        # log.debug(f"CapsNet.margin_loss(): capsnet_output.shape = {capsnet_output.shape}")
        # log.debug(f"CapsNet.margin_loss(): labels = {labels}")
        # log.debug(f"CapsNet.margin_loss(): labels.shape = {labels.shape}")

        batch_size = capsnet_output.size(0)
        # log.debug(f"CapsNet.margin_loss(): batch_size = {batch_size}")

        # calculate the norm of the batch of all output vectors of the capsnet
        v_c = torch.sqrt((capsnet_output ** 2).sum(dim=2, keepdim=True))
        # log.debug(f"CapsNet.margin_loss(): v_c.shape = {v_c.shape}")

        # TODO do I need to make this inplace?
        # TODO check result of removing the square part out of the loss function
        present_error = F.relu(m_plus - v_c)
        # log.debug(f"CapsNet.margin_loss(): present_error.shape = {present_error.shape}")
        # TODO verify if this is necessary
        present_error = present_error.view(batch_size, -1)
        # log.debug(f"CapsNet.margin_loss(): present_error.shape = {present_error.shape}")

        # TODO do I need to make this inplace?
        # TODO check result of removing the square part out of the loss function
        absent_error = F.relu(v_c - m_minus)
        # log.debug(f"CapsNet.margin_loss(): absent_error.shape = {absent_error.shape}")
        # TODO verify if this is necessary
        absent_error = absent_error.view(batch_size, -1)
        # log.debug(f"CapsNet.margin_loss(): absent_error.shape = {absent_error.shape}")

        loss = labels * present_error + lambda_val * (1.0 - labels) * absent_error
        # log.debug(f"CapsNet.margin_loss(): loss.shape = {loss.shape}")
        loss = loss.sum(dim=1).mean()
        # log.debug(f"CapsNet.margin_loss(): loss.shape = {loss.shape}")

        assert not torch.isnan(loss)
        return loss

    def reconstruction_loss(self, input_to_capsnet, reconstructions):
        assert not torch.isnan(input_to_capsnet).any()
        assert not torch.isnan(reconstructions).any()
        loss = self.mse_loss(reconstructions.view(reconstructions.size(0), -1),
                input_to_capsnet.view(reconstructions.size(0), -1))
        # log.debug(f"RL: loss.shape = {loss.shape}")
        assert not torch.isnan(loss)
        return loss

    def loss(self,
            capsnet_output,
            input_to_capsnet,
            target,
            reconstructions,
            alpha_val=0.0005):
        loss_val = self.margin_loss(capsnet_output, target) + self.reconstruction_loss(
                input_to_capsnet, reconstructions) * alpha_val
        assert not torch.isnan(loss_val)
        return loss_val

    def test_naive(self, x):
        # TODO replace prints with logs
        log.debug("Testing CapsNet")
        log.debug(f"CX: x.shape = {x.shape}")
        u, reconstructions, masked = self.forward(x)
        log.debug(f"CY: u.shape = {u.shape}")
        # CY: u.shape = torch.Size([1, 10, 16, 1])
        log.debug(f"CY: reconstructions.shape = {reconstructions.shape}")
        # CY: reconstructions.shape = torch.Size([1, 1, 28, 28])
        log.debug(f"CY: masked.shape = {masked.shape}")
        # CY: masked.shape = torch.Size([1, 10])
        return u, masked

    def train_epoch(self, optimizer, train_loader, epoch_index, device):
        """
        Train for one epoch, whose index is given
        Also save it
        """
        num_batch = len(train_loader)

        total_loss = 0
        for batch_id, (data, target) in enumerate(tqdm(train_loader)):
            data = data.to(device)
            target = target.to(device)
            # log.info(f"data.device = {data.device}")
            # log.info(f"target.device = {target.device}")
            start = time.perf_counter()
            # log.debug(f"Tr: target = {target}")

            if self.index_one:
                # use index 0 instead of index 1
                target = target - 1

            # target = torch.sparse.torch.eye(10).index_select(dim=0, index=target)
            target = torch.sparse.torch.eye(
                    self.num_classes).to(device).index_select(dim=0, index=target)
            # log.debug(f"Tr: target = {target}")
            # log.debug(f"Tr: target.shape = {target.shape}")
            # log.debug(f"Tr: target.device = {target.device}")
            input_data, target = Variable(data), Variable(target)
            # log.debug(f"Tr: input_data.shape = {input_data.shape}")
            # log.debug(f"Tr: input_data.device = {input_data.device}")
            # log.debug(f"Tr: target.shape = {target.shape}")
            # log.debug(f"Tr: target.device = {target.device}")

            # TODO CUDA support

            optimizer.zero_grad()

            prof_activities = [ProfilerActivity.CPU]
            if device == torch.device('cuda'):
                prof_activities.append(ProfilerActivity.CUDA)
            with profile(activities=prof_activities, profile_memory=True, record_shapes=True) as prof:
                output, reconstructions, masked = self.forward(input_data)
            loss = self.loss(output, input_data, target, reconstructions)
            # log.debug(f"Tr: loss.shape = {loss.shape}")
            # log.debug(f"Tr: loss.device = {loss.device}")
            # get gradients
            loss.backward()
            assert not torch.isnan(loss).any()
            # Update parameters
            optimizer.step()

            batch_of_index_of_output_class = np.argmax(masked.data.cpu().numpy(), 1)
            batch_of_index_of_target_class = np.argmax(target.data.cpu().numpy(), 1)
            num_correct = sum(batch_of_index_of_output_class == batch_of_index_of_target_class)

            train_loss = loss.item()
            total_loss = total_loss + train_loss
            total_samples = target.size(0)
            accuracy = num_correct / total_samples
            # log scalars for guildai
            end = time.perf_counter()
            if device == torch.device("cuda"):
                log.scalars({
                    "accuracy": accuracy,
                    "loss": train_loss,
                    "epoch": epoch_index,
                    "time": end - start,
                    "parameters": self.num_params(),
                    "self_CPU_mem_use_in_bits": prof_helper.get_total_mem_usage(prof),
                    "self_GPU_mem_use_in_bits": prof_helper.get_total_cuda_mem_usage(prof),
                    })
            else:
                log.scalars({
                    "accuracy": accuracy,
                    "loss": train_loss,
                    "epoch": epoch_index,
                    "time": end - start,
                    "parameters": self.num_params(),
                    "self_CPU_mem_use_in_bits": prof_helper.get_total_mem_usage(prof),
                    })

            if batch_id % 100 == 0:
                tqdm.write("epoch_index: {}, batch: {}, accuracy: {:.6f}, loss: {:.6f}".format(
                    epoch_index,
                    batch_id,
                    num_correct / total_samples,
                    train_loss,
                    ))
        
        tqdm.write(f"epoch_index: {epoch_index}, epoch_index_loss: {total_loss}")
        # log.info(f"saved checkpoint")
        # self.save_checkpoint(epoch_index, optimizer, total_loss)

    def test_epoch(self, test_loader, epoch_index, device):
        test_loss = 0
        accuracy = []
        times = []
        CPU_mem = []
        CUDA_mem = []
        for batch_id, (data, target) in enumerate(tqdm(test_loader)):
            data = data.to(device)
            target = target.to(device)
            # log.info(f"Te: data.device = {data.device}")
            # log.info(f"Te: target.device = {target.device}")

            if self.index_one:
                # use index 0 instead of index 1
                target = target - 1

            target = torch.sparse.torch.eye(
                    self.num_classes).to(device).index_select(dim=0, index=target)
            # log.debug(f"Te: target = {target}")
            # log.debug(f"Te: target.shape = {target.shape}")
            # log.debug(f"Te: target.device = {target.device}")
            input_data, target = Variable(data), Variable(target)
            # log.debug(f"Te: input_data.shape = {input_data.shape}")
            # log.debug(f"Te: input_data.device = {input_data.device}")
            # log.debug(f"Te: target.shape = {target.shape}")
            # log.debug(f"Te: target.device = {target.device}")

            start = time.perf_counter()
            prof_activities = [ProfilerActivity.CPU]
            if device == torch.device('cuda'):
                prof_activities.append(ProfilerActivity.CUDA)
            with profile(activities=prof_activities, profile_memory=True, record_shapes=True) as prof:
                output, reconstructions, masked = self.forward(input_data)
            end = time.perf_counter()
            times.append(end - start)

            loss = self.loss(output, input_data, target, reconstructions)

            test_loss = test_loss + loss.item()
            batch_of_index_of_output_class = np.argmax(masked.data.cpu().numpy(), 1)
            batch_of_index_of_target_class = np.argmax(target.data.cpu().numpy(), 1)

            num_correct = sum(batch_of_index_of_output_class == batch_of_index_of_target_class)
            total_samples = target.size(0)
            accuracy.append(num_correct / total_samples)
            CPU_mem.append(prof_helper.get_total_mem_usage(prof))
            if device == torch.device("cuda"):
                CUDA_mem.append(prof_helper.get_total_cuda_mem_usage(prof))

        average_accuracy = sum(accuracy) / len(accuracy)
        average_time = sum(times) / len(times)
        tqdm.write(f"epoch_index: {epoch_index}, \
                epoch_index_loss: {test_loss}, \
                accuracy: {average_accuracy}")
        if device == torch.device("cuda"):
            log.scalars_noinc({
                "test_epoch": epoch_index,
                "test_accuracy": average_accuracy,
                "test_loss": test_loss,
                "test_time": average_time,
                # batch size differs, also no optimization
                # therefore logging mem use may be useful
                "test_avg_self_CPU_mem_use_in_bits": sum(CPU_mem)/len(CPU_mem),
                "test_avg_self_GPU_mem_use_in_bits": sum(CUDA_mem)/len(CUDA_mem),
                })
        else:
            log.scalars_noinc({
                "test_epoch": epoch_index,
                "test_accuracy": average_accuracy,
                "test_loss": test_loss,
                "test_time": average_time,
                "test_avg_self_CPU_mem_use_in_bits": sum(CPU_mem)/len(CPU_mem),
                })

    def save_checkpoint(self, epoch, optimizer, loss, path="trained.tar"):
        """
        Save a checkpoint for inference or continued training later
        """
        torch.save({
            "epoch": epoch,
            "model_state_dict": self.state_dict(),
            "optimizer_state_dict": optimizer.state_dict(),
            "loss": loss,
            }, path)

    def load_checkpoint(self, optimizer, path="trained.tar"):
        """
        Load a checkpoint for inference or continued training
        """
        checkpoint = torch.load(path)
        self.load_state_dict(checkpoint["model_state_dict"])
        optimizer.load_state_dict(checkpoint["optimizer_state_dict"])
        epoch = checkpoint["epoch"]
        loss = checkpoint["loss"]
        return epoch, optimizer, loss

    def num_params(self):
        num = 0
        num = num + self.conv_layer.num_params()
        num = num + self.primary_caps_layer.num_params()
        num = num + self.decoder_network.num_params()
        return num


def test():
    # test all classes
    conv_layer = ConvLayer()
    primary_layer = PrimaryCapsLayer()
    final_layer = FinalCapsLayer()
    decoder = Decoder()
    capsnet = CapsNet()
    x = torch.rand(1, 1, 28, 28)
    y = conv_layer.test(x)
    z = primary_layer.test(y)
    a = final_layer.test(z)
    b, masked = decoder.test(a)

    x = torch.rand(1, 1, 28, 28)
    u, masked = capsnet.test_naive(x)
    capsnet.margin_loss(u, torch.tensor([[2]]))

if __name__ == "__main__":
    # the test is rather compute intensive, now that VBMF is part of it
    # test()
    pass
