# MIT License; since based on code by einops author:
# https://github.com/arogozhnikov/readable_capsnet/blob/master/capsnet.py
import torch
import torch.nn as nn
from einops.layers.torch import WeightedEinsum, Rearrange
import einops
import log
import time

def squash(x, dim, epsilon=1e-8):
    # keepdim=True ensures that the output is tensor([num]) and not tensor(num)
    # if you don't use epsilon term, you'll get an undefined gradient due to
    # division by zero when torch.sqrt(squared_norm) equals zero
    # see issue: https://github.com/jindongwang/Pytorch-CapsuleNet/issues/7
    squared_norm = ((x ** 2) + epsilon).sum(dim, keepdim=True)
    norm = torch.sqrt(squared_norm)
    return (squared_norm / (1.0 + squared_norm)) * (x / norm)

def margin_loss(class_capsules, target_one_hot, m_minus=0.1, m_plus=0.9, loss_lambda=0.5):
    caps_norms = torch.norm(class_capsules, dim=2)
    assert caps_norms.max() <= 1.001, "The outputs of capsules should be bound by unit norm"
    loss_sig = torch.clamp(m_plus - caps_norms, 0) ** 2
    loss_bkg = torch.clamp(caps_norms - m_minus, 0) ** 2

    loss = target_one_hot * loss_sig + loss_lambda * (1.0 - target_one_hot) * loss_bkg
    return einops.reduce(loss, "b cls -> b", "sum")


class CapsuleLayerWithRouting(nn.Module):
    def __init__(self,
            num_in_caps,
            dim_in_caps,
            num_out_caps,
            dim_out_caps,
            ):
        # TODO I still haven't figured out why this is necessary
        super().__init__()
        # TODO explain what this does
        self.input_caps_to_U = WeightedEinsum(
                "b num_in_caps dim_in_caps -> b num_in_caps num_out_caps dim_out_caps",
                weight_shape="num_in_caps dim_in_caps num_out_caps dim_out_caps",
                num_in_caps=num_in_caps,
                dim_in_caps=dim_in_caps,
                num_out_caps=num_out_caps,
                dim_out_caps=dim_out_caps,
                )

    def forward(self, input_capsules, routing_iterations=3):
        U = self.input_caps_to_U(input_capsules)
        batch, num_in_caps, num_out_caps, dim_out_caps = U.shape

        # ???
        B = torch.zeros([batch, num_in_caps, num_out_caps], device=U.device)

        # routing
        for _ in range(routing_iterations):
            C = torch.softmax(B, dim=-1)
            S = torch.einsum("bio, bioh -> boh", C, U)
            V = squash(S, dim=-1)
            B = B + torch.einsum("bioh, boh -> bio", U, V)
        return V


class Encoder(nn.Module):
    """
    Takes image as input, passes it through primary capsules and digit capsules and returns the output
    """
    def __init__(self,
            in_height,
            in_width,
            in_channels,
            num_primary_caps,
            dim_primary_caps,
            num_digit_caps,
            dim_digit_caps,
            ):
        super().__init__()
        self.image_to_primary_caps = nn.Sequential(
                # why 256
                nn.Conv2d(in_channels, 256, kernel_size=9),
                nn.ReLU(inplace=True),
                nn.Conv2d(256, num_primary_caps * dim_primary_caps, kernel_size=9, stride=2),
                Rearrange(
                    "b (num_caps dim_caps) h w -> b (h w num_caps) dim_caps",
                    num_caps=num_primary_caps,
                    dim_caps=dim_primary_caps,
                    ),
                )
        # figure out correct number of primary caps by a test
        _, test_num_primary_caps, _ = self.image_to_primary_caps(
                torch.zeros(1, in_channels, in_height, in_width)).shape
        self.primary_to_digit_capsules = CapsuleLayerWithRouting(
                num_in_caps=test_num_primary_caps,
                dim_in_caps=dim_primary_caps,
                num_out_caps=num_digit_caps,
                dim_out_caps=dim_digit_caps,
                )

    def forward(self, images, routing_iterations=3):
        # TODO do you really need to scale this?
        primary_capsules = self.image_to_primary_caps(images) * 0.01
        return self.primary_to_digit_capsules(primary_capsules, routing_iterations)


def Decoder(
        num_caps,
        dim_caps,
        out_height,
        out_width,
        out_channels,
        ):
    """
    Returns a nn.Sequential object that decodes the output of Encoder to reconstruct the input
    """
    return nn.Sequential(
            WeightedEinsum(
                "b num_caps dim_caps -> b hidden",
                weight_shape="num_caps dim_caps hidden",
                num_caps=num_caps,
                dim_caps=dim_caps,
                hidden=512,
                ),
            nn.ReLU(inplace=True),
            nn.Linear(512, 1024),
            nn.ReLU(inplace=True),
            WeightedEinsum(
                "b hidden -> b c h w",
                weight_shape="hidden c h w",
                hidden=1024,
                c=out_channels,
                h=out_height,
                w=out_width,
                ),
            nn.Sigmoid(),
            )


class CapsNet(nn.Module):
    def __init__(self,
            encoder=Encoder(
                in_height=28,
                in_width=28,
                in_channels=1,
                num_primary_caps=32,
                dim_primary_caps=8,
                num_digit_caps=10,
                dim_digit_caps=16,
                ),
            decoder=Decoder(
                num_caps=10,
                dim_caps=16,
                out_height=28,
                out_width=28,
                out_channels=1,
                ),
            ):
        # without this some error occurs
        super(CapsNet, self).__init__()

        self.encoder = encoder
        self.decoder = decoder

    def num_params(self):
        # get encoder params
        encoder_params = sum([p.numel() for p in self.encoder.image_to_primary_caps.parameters()])
        encoder_params = encoder_params + sum(
                [p.numel() for p in self.encoder.primary_to_digit_capsules.input_caps_to_U.parameters()])
        # get decoder params
        decoder_params = sum([p.numel() for p in self.decoder.parameters()])
        return encoder_params + decoder_params

    def train_epoch(self, optimizer, train_loader, epoch_index):
        new_batch = len(list(enumerate(train_loader)))
        total_loss = 0
        for batch_id, (images, labels) in enumerate(train_loader):
            labels_one_hot = torch.nn.functional.one_hot(labels, num_classes=10).float()

            start = time.perf_counter()
            digit_capsules = self.encoder(images)
            reconstructed = self.decoder(digit_capsules * einops.rearrange(labels_one_hot, "b caps -> b caps 1"))
            end = time.perf_counter()

            loss = margin_loss(digit_capsules, labels_one_hot).mean()
            reconstruction_loss_mse = (images - reconstructed).pow(2).mean()
            loss = loss + reconstruction_loss_mse * 0.0005
            loss.backward()
            total_loss = total_loss + loss.detach().item()

            optimizer.step()
            optimizer.zero_grad()

            predicted_labels = digit_capsules.norm(dim=2).argmax(dim=1)
            accuracy = torch.sum(predicted_labels == labels) / len(labels)

            log.scalars({
                "epoch": epoch_index,
                "time": end - start,
                "loss": loss,
                "accuracy": accuracy,
                "parameters": self.num_params(),
                })

        # TODO save checkpoint; which means implement saving
