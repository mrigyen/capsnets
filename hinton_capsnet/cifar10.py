import os
import torchvision
import capsnet
import torch
import capsnet
import log

DATA_DIR = os.environ.get("DATA_DIR") or "/tmp"
batch_size = 128
learning_rate = 0.001
epochs = 3
USE_GPU = True

if USE_GPU and torch.cuda.device_count() > 0:
    device = torch.device('cuda')
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    # device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')
log.info(f"torch.device = {device}")

def get_cifar():
    CIFAR = torchvision.datasets.CIFAR10
    data_path = os.path.join(DATA_DIR, "CIFAR")

    # mean and std values equal to that used here
    # https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html
    mean, std = ((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))

    transforms = torchvision.transforms.Compose([
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean, std),
        ])

    trainset = CIFAR(root=data_path, download=True, train=True, transform=transforms)
    train_loader = torch.utils.data.DataLoader(
            trainset,
            batch_size=batch_size,
            # shuffle=True causes the following error (../docs/shuffle_error.log)when training on GPU
            shuffle=False,
            # "num_workers (int, optional) – how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process. (default: 0)"
            num_workers=0,
            )

    testset = CIFAR(root=data_path, download=True, train=False, transform=transforms)
    test_loader = torch.utils.data.DataLoader(
            testset,
            batch_size=batch_size,
            shuffle=False,
            # "num_workers (int, optional) – how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process. (default: 0)"
            num_workers=0,
            )
    return train_loader, test_loader

if __name__ == "__main__":
    train_loader, test_loader = get_cifar()
    num_routes = 32 * 8 * 8
    caps_net = capsnet.CapsNet(
            conv_layer=capsnet.ConvLayer(in_channels=3),
            primary_caps_layer=capsnet.PrimaryCapsLayer(
                num_routes=num_routes,
                ),
            final_caps_layer=capsnet.FinalCapsLayer(
                num_routes=num_routes,
                ),
            decoder_network=capsnet.Decoder(
                output_width=32,
                output_height=32,
                output_channels=3,
                ),
            ) 
    caps_net.to(device)
    optimizer = torch.optim.Adam(caps_net.parameters(), lr=learning_rate)
    for i in range(epochs):
        caps_net.train_epoch(optimizer, train_loader, i, device)
        # caps_net.test_epoch(test_loader, i)
        # if i == 0:
        #     caps_net.primary_caps_layer.tucker_decompose_layers()
        # if i == 1:
        #     caps_net.decoder_network.svd_decompose_layers()
