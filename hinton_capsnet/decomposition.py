from VBMF import VBMF
import torch
import torch.nn as nn
import log
import numpy as np
import tenalg
import tendec

def estimate_ranks(conv_layer):
    """
    https://github.com/jacobgil/pytorch-tensor-decompositions/blob/master/decompositions.py
    """
    weights = conv_layer.weight.data
    # unfold along the first dimension and get a 2D matrix
    unfold_zero = tenalg.unfold(weights, 0)

    # unfold along the second dimension and get a 2D matrix
    # TODO why unfold along the second dimension?
    unfold_one = tenalg.unfold(weights, 1)

    # get EVBMF sigma (diagonal matrix of singular values)
    _, diag_zero, _, _ = VBMF.EVBMF(unfold_zero)
    _, diag_one, _, _ = VBMF.EVBMF(unfold_one)

    # TODO Why send different indices of the shape as ranks?
    ranks = [diag_zero.shape[0], diag_one.shape[1]]
    return ranks

def halve_ranks(conv_layer):
    weights = conv_layer.weight.data
    # unfold along the first dimension and get a 2D matrix
    unfold_zero = tenalg.unfold(weights, 0)

    # unfold along the second dimension and get a 2D matrix
    # TODO why unfold along the second dimension?
    unfold_one = tenalg.unfold(weights, 1)

    # get EVBMF sigma (diagonal matrix of singular values)
    rank_zero = torch.round(torch.linalg.matrix_rank(unfold_zero) / 2)
    rank_one = torch.round(torch.linalg.matrix_rank(unfold_one) / 2)

    # TODO Why send different indices of the shape as ranks?
    ranks = [int(rank_zero.item()), int(rank_one.item())]
    return ranks

def quarter_ranks(conv_layer):
    weights = conv_layer.weight.data
    # unfold along the first dimension and get a 2D matrix
    unfold_zero = tenalg.unfold(weights, 0)

    # unfold along the second dimension and get a 2D matrix
    # TODO why unfold along the second dimension?
    unfold_one = tenalg.unfold(weights, 1)

    # get EVBMF sigma (diagonal matrix of singular values)
    rank_zero = torch.round(torch.linalg.matrix_rank(unfold_zero) / 4)
    rank_one = torch.round(torch.linalg.matrix_rank(unfold_one) / 4)

    # TODO Why send different indices of the shape as ranks?
    ranks = [int(rank_zero.item()), int(rank_one.item())]
    return ranks

def decimate_ranks(conv_layer):
    weights = conv_layer.weight.data
    # unfold along the first dimension and get a 2D matrix
    unfold_zero = tenalg.unfold(weights, 0)

    # unfold along the second dimension and get a 2D matrix
    # TODO why unfold along the second dimension?
    unfold_one = tenalg.unfold(weights, 1)

    # get EVBMF sigma (diagonal matrix of singular values)
    rank_zero = torch.round(torch.linalg.matrix_rank(unfold_zero) / 10)
    rank_one = torch.round(torch.linalg.matrix_rank(unfold_one) / 10)

    # TODO Why send different indices of the shape as ranks?
    ranks = [int(rank_zero.item()), int(rank_one.item())]
    return ranks

def tucker_decomposition_conv_layer(conv_layer):
    """
    input: Conv2D layer
    returns: a nn.Sequential object with Tucker decomposition applied on the
    conv_layer
    """
    # use EVBMF to estimate ranks
    # ranks = estimate_ranks(conv_layer)
    # ranks = halve_ranks(conv_layer)
    # ranks = quarter_ranks(conv_layer)
    # ranks = decimate_ranks(conv_layer)
    ranks = [1, 1]
    log.info(f"\n{conv_layer}\nVBMF Estimated ranks: {ranks}")

    # fix to prevent error that occurs when you send [0, 0] as ranks to partial_tucker
    if ranks == [0, 0]:
        log.info("Invalid ranks {ranks}, setting to [1, 1]")
        ranks = [1, 1]

    # decomposes conv_layer.weight.data into a Tucker decomposition exclusively along the provided modes
    # init="svd" is the default option anyway
    # TODO but why only mode 0 and 1?
    core, [last, first] = tendec.partial_tucker(
            conv_layer.weight.data,
            modes=[0, 1],
            rank=ranks,
            init="svd")
    log.debug(f"core.shape = {core.shape}")
    log.debug(f"first.shape = {first.shape}")
    log.debug(f"last.shape = {last.shape}")

    # pointwise convolution that reduces the channels from S to R3
    first_layer = torch.nn.Conv2d(
            in_channels=first.shape[0],
            out_channels=first.shape[1],
            kernel_size=1,
            stride=1,
            padding=0,
            dilation=conv_layer.dilation,
            bias=False,
            )
    log.debug(f"first_layer = {first_layer}")
    log.debug(f"first_layer.weight.shape = {first_layer.weight.shape}")

    # regular 2D convolution with R3 input channels and R3 output channels
    core_layer = torch.nn.Conv2d(
            in_channels=core.shape[1],
            out_channels=core.shape[0],
            kernel_size=conv_layer.kernel_size,
            stride=conv_layer.stride,
            padding=conv_layer.padding,
            dilation=conv_layer.dilation,
            bias=False,
            )
    log.debug(f"core_layer = {core_layer}")
    log.debug(f"core_layer.weight.shape = {core_layer.weight.shape}")

    # pointwise convolution that increases the channels R4 to T
    last_layer = torch.nn.Conv2d(
            in_channels=last.shape[1],
            out_channels=last.shape[0],
            kernel_size=1,
            stride=1,
            padding=0,
            dilation=conv_layer.dilation,
            bias=True,
            )
    log.debug(f"last_layer = {last_layer}")
    log.debug(f"last_layer.weight.shape = {last_layer.weight.shape}")
    last_layer.bias.data = conv_layer.bias.data

    log.debug(f"first_layer.weight.shape = {first_layer.weight.shape}")
    first_layer.weight.data = torch.transpose(first, 1, 0).unsqueeze(-1).unsqueeze(-1)
    log.debug(f"first_layer.weight.shape = {first_layer.weight.shape}")
    core_layer.weight.data = core
    log.debug(f"core_layer.weight.shape = {core_layer.weight.shape}")
    last_layer.weight.data = last.unsqueeze(-1).unsqueeze(-1)
    log.debug(f"last_layer.weight.shape = {last_layer.weight.shape}")

    new_layers = [first_layer, core_layer, last_layer]
    return nn.Sequential(*new_layers)

def svd_decomposition_fnn_layer(fnn_layer):
    weight = fnn_layer.weight.data
    # U, sigma_diag, Vh = torch.linalg.svd(weight, full_matrices=False)
    rank = torch.linalg.matrix_rank(weight)
    U, sigma_diag, Vh = tendec.partial_svd(weight, n_eigenvecs=1)
    sigma = torch.eye(sigma_diag.shape[0]) * sigma_diag

    # create the first FNN layer for sigma * Vh
    first_layer_weight = torch.matmul(sigma, Vh)
    in_features = first_layer_weight.shape[1]
    out_features = first_layer_weight.shape[0]
    first_layer = nn.Linear(
            in_features,
            out_features,
            bias=False, # bias=False because it doesn't make sense to have biases in between SVD layers
            )
    first_layer.weight.data = first_layer_weight

    # create the second FNN layer for U
    in_features = U.shape[1]
    out_features = U.shape[0]
    second_layer = nn.Linear(
            in_features,
            out_features,
            bias=True,
            )
    second_layer.weight.data = U
    # TODO this may be fitting a tensor to an incorrect shape
    assert second_layer.bias.data.shape == fnn_layer.bias.data.shape
    second_layer.bias.data = fnn_layer.bias.data

    new_layers = [first_layer, second_layer]
    return nn.Sequential(*new_layers)

if __name__ == "__main__":
    # test the SVD thing
    input = torch.rand(100)
    fnn_layer = nn.Linear(100, 50)
    standard_output = fnn_layer(input)
    svd_layers = svd_decomposition_fnn_layer(fnn_layer)
    svd_network_output = svd_layers(input)
    error = standard_output - svd_network_output
    error = error.sum()
    print(error)
