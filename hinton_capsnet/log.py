#!/usr/bin/env python3
"""
Minimalist and sane interface with the PEP8 breaking (and non idempotent) logging STL module
"""
import logging
import os
import datetime

LOG_DIR = os.environ.get("LOG_DIR") or "./" 

filename = "/log_" + str(datetime.datetime.now()) + ".tsv"

log_format = "%(asctime)s\t%(levelname)s\t%(message)s"

logging.basicConfig(
        level=logging.DEBUG,
        filename=LOG_DIR+filename,
        format=log_format,
        )

def info(string):
    print(string)
    logging.info(string)

# workaround because guildai sets logging level higher than debug making it
# harder to log this
debug = logging.critical

info("New run initiated")

step = 0

def scalars(scalars_dict):
    """
    Log scalars for guildai / tensorboard
    """
    global step
    assert step is not None
    info(f"step: {step}")
    step = step + 1
    for scalar in scalars_dict:
        info(f"{scalar}: {scalars_dict[scalar]}")

def scalars_noinc(scalars_dict):
    """
    Log scalars for guildai / tensorboard
    Do not increment the step
    """
    global step
    assert step is not None
    info(f"step: {step}")
    for scalar in scalars_dict:
        info(f"{scalar}: {scalars_dict[scalar]}")
