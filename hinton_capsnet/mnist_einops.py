import os
import torchvision
import torch
from capsnet_einops import CapsNet

DATA_DIR = os.environ.get("DATA_DIR") or "/tmp"
batch_size = 128
learning_rate = 0.001
epochs = 3

def get_mnist():
    MNIST = torchvision.datasets.MNIST
    data_path = os.path.join(DATA_DIR, "MNIST")
    classes = list(range(10))
    # TODO why are these values used?
    mean, std = ((0.1307,), (0.3081,))

    transforms = torchvision.transforms.Compose([
        # TODO figure out what "images that have been shifted by upto 2 pixels
        # in each direction with zero padding."
        # TODO is this necessary
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean, std)
        ])

    trainset = MNIST(root=data_path, train=True, download=True, transform=transforms)
    train_loader = torch.utils.data.DataLoader(trainset,
            batch_size=batch_size,
            shuffle=True,
            # TODO What is this for?
            num_workers=2)
    testset = MNIST(root=data_path, train=False, download=True, transform=transforms)
    test_loader = torch.utils.data.DataLoader(testset,
            batch_size=batch_size,
            shuffle=False,
            # TODO What is this for?
            num_workers=2)
    return train_loader, test_loader

if __name__ == "__main__":
    train_loader, test_loader = get_mnist()
    caps_net = CapsNet() 
    optimizer = torch.optim.Adam(caps_net.parameters(), lr=learning_rate)
    for i in range(epochs):
        caps_net.train_epoch(optimizer, train_loader, i)
        # caps_net.test_epoch(test_loader, i)
        # if i == 0:
        #     caps_net.primary_caps_layer.tucker_decompose_layers()
        # if i == 1:
        #     caps_net.decoder_network.svd_decompose_layers()
