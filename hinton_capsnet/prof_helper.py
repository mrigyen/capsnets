import log

def get_total_mem_usage(prof):
    table = prof.key_averages().table()

    # remove empty lines
    table = table.split("\n")
    table = [line for line in table if line != ""]

    # remove top --- lines
    table = table[1:]
    table = table[0:1] + table[2:]

    # remove the CPU time and the bottom --- line
    table = table[:-2]
    
    # remove all prefixed and suffixed whitespace
    table = [line.strip() for line in table]

    # convert list into list of lists, while removing empty entries in a line entry
    table = [[element.strip() for element in line.split("  ") if element != ""] for line in table]

    # remove the [memory] entry, which is negative for some reason
    table = [entry for entry in table if entry[0] != "[memory]"]

    # get cpu_mem column index
    mem_index = None
    header = table[0]
    for i, head in enumerate(header):
        if head == "Self CPU Mem":
            mem_index = i
    if mem_index == None:
        import sys
        print("'Self CPU Mem' column not found")
        sys.quit()

    # drop the header
    table = table[1:]

    # drop the last line of ----
    if table[-1][0][:2] == "--":
        table = table[:-1]

    # print(table)

    # Get sum of Self CPU Mem use
    total_memory_use = sum([str_to_mem_in_b(entry[mem_index]) for entry in table])
    return total_memory_use

def get_total_cuda_mem_usage(prof):
    table = prof.key_averages().table()

    # remove empty lines
    table = table.split("\n")
    table = [line for line in table if line != ""]

    # remove top --- lines
    table = table[1:]
    table = table[0:1] + table[2:]

    # remove the CPU time and the bottom --- line
    table = table[:-2]
    
    # remove all prefixed and suffixed whitespace
    table = [line.strip() for line in table]

    # convert list into list of lists, while removing empty entries in a line entry
    table = [[element.strip() for element in line.split("  ") if element != ""] for line in table]

    # remove the [memory] entry, which is negative for some reason
    table = [entry for entry in table if entry[0] != "[memory]"]

    # get cpu_mem column index
    mem_index = None
    header = table[0]
    for i, head in enumerate(header):
        if head == "Self CUDA Mem":
            mem_index = i
    if mem_index == None:
        import sys
        print("'Self CUDA Mem' column not found")
        sys.quit()

    # drop the header
    table = table[1:]

    # drop the last line of ----
    if table[-1][0][:2] == "--":
        table = table[:-1]

    # Get sum of Self CUDA Mem use
    total_memory_use = sum([str_to_mem_in_b(entry[mem_index]) for entry in table])
    return total_memory_use


def str_to_mem_in_b(mem_str: str) -> float:
    # log.info(f"mem_str = {mem_str}")
    number, denomination = mem_str.split()
    number = float(number)
    
    if denomination == "b":
        return number
    # kilobits Kb
    elif denomination == "Kb":
        return number * pow(10, 3)
    elif denomination == "Mb":
        return number * pow(10, 6)
    elif denomination == "Gb":
        return number * pow(10, 9)
    else:
        raise ValueError("Unknown denomination used for memory")
