import os
import torchvision
import capsnet
import torch
import capsnet
import log
from smallnorb_dataset import *

DATA_DIR = os.environ.get("DATA_DIR") or "/mnt/WD_2TB/datasets/kaggle/smallNORB/"
batch_size = 128
learning_rate = 0.001
epochs = 3
USE_GPU = True

if USE_GPU and torch.cuda.device_count() > 0:
    device = torch.device('cuda')
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    # device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')
log.info(f"torch.device = {device}")

def common_transform(data):
    """ Common transform applied to the dataset """
    res = np.zeros((data.size(0), 2, 48, 48), dtype='uint8')
    for i, item in enumerate(data):
        item = item.numpy()
        img0 = Image.fromarray(item[0], mode='L').resize((48, 48))
        img1 = Image.fromarray(item[1], mode='L').resize((48, 48))
        res[i] = np.asarray([np.asarray(img0), np.asarray(img1)])
    return torch.from_numpy(res)

def get_smallnorb():
    train_transform = torchvision.transforms.Compose([
        torchvision.transforms.RandomCrop(32),
        torchvision.transforms.ColorJitter(brightness=0.2, contrast=0.5),
        torchvision.transforms.ToTensor(),
        # torchvision.transforms.Normalize([0.177, 0.174], [0.752, 0.757]),
    ])
    test_transform = torchvision.transforms.Compose([
        torchvision.transforms.CenterCrop(32),
        torchvision.transforms.ToTensor(),
        # torchvision.transforms.Normalize([0.177, 0.174], [0.752, 0.757]),
    ])

    trainset = smallNORB(root=DATA_DIR, download=True, train=True, transform=train_transform)
    trainset.train_data = common_transform(trainset.train_data)
    train_loader = torch.utils.data.DataLoader(
            trainset,
            batch_size=batch_size,
            # shuffle=True causes the following error (../docs/shuffle_error.log)when training on GPU
            shuffle=False,
            # "num_workers (int, optional) – how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process. (default: 0)"
            num_workers=0,
            )

    testset = smallNORB(root=DATA_DIR, download=True, train=False, transform=test_transform)
    testset.test_data = common_transform(testset.test_data)
    test_loader = torch.utils.data.DataLoader(
            testset,
            batch_size=batch_size,
            shuffle=False,
            # "num_workers (int, optional) – how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process. (default: 0)"
            num_workers=0,
            )
    return train_loader, test_loader

if __name__ == "__main__":
    train_loader, test_loader = get_smallnorb()
    num_routes = 32 * 8 * 8
    caps_net = capsnet.CapsNet(
            conv_layer=capsnet.ConvLayer(in_channels=2),
            primary_caps_layer=capsnet.PrimaryCapsLayer(
                num_routes=num_routes,
                ),
            final_caps_layer=torch.nn.DataParallel(capsnet.FinalCapsLayer(
                num_routes=num_routes,
                )),
            decoder_network=capsnet.Decoder(
                output_width=32,
                output_height=32,
                output_channels=2,
                ),
            ) 
    caps_net.to(device)
    optimizer = torch.optim.Adam(caps_net.parameters(), lr=learning_rate)
    for i in range(epochs):
        caps_net.train_epoch(optimizer, train_loader, i, device)
        # caps_net.test_epoch(test_loader, i)
        # if i == 0:
        #     caps_net.primary_caps_layer.tucker_decompose_layers()
        # if i == 1:
        #     caps_net.decoder_network.svd_decompose_layers()
