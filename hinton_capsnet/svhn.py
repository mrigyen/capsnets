import os
import torchvision
import capsnet
import torch
import log

DATA_DIR = os.environ.get("DATA_DIR") or "/tmp"
batch_size = 128
learning_rate = 0.001
epochs = 3
USE_GPU = True

if USE_GPU and torch.cuda.device_count() > 0:
    device = torch.device('cuda')
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    # device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device('cpu')
log.info(f"torch.device = {device}")

def get_svhn():
    SVHN = torchvision.datasets.SVHN
    data_path = os.path.join(DATA_DIR, "SVHN")
    # TODO why are these values used?
    mean, std = ((0.1307,), (0.3081,))

    transforms = torchvision.transforms.Compose([
        # TODO figure out what "images that have been shifted by upto 2 pixels
        # in each direction with zero padding."
        # TODO is this necessary
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean, std)
        ])

    trainset = SVHN(
            root=data_path,
            split="train",
            transform=transforms,
            download=True,
            )
    train_loader = torch.utils.data.DataLoader(
            trainset,
            batch_size=batch_size,
            # shuffle=True causes the following error (../docs/shuffle_error.log)when training on GPU
            shuffle=False,
            # "num_workers (int, optional) – how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process. (default: 0)"
            num_workers=0,
            )

    testset = SVHN(
            root=data_path,
            download=True,
            split="test",
            transform=transforms,
            )
    test_loader = torch.utils.data.DataLoader(
            testset,
            batch_size=batch_size,
            shuffle=False,
            # "num_workers (int, optional) – how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process. (default: 0)"
            num_workers=0,
            )
    # trainset.train_data.to(device)
    # trainset.train_labels.to(device)
    # testset.train_data.to(device)
    # testset.train_labels.to(device)
    return train_loader, test_loader

if __name__ == "__main__":
    train_loader, test_loader = get_svhn()
    num_routes = 32 * 8 * 8
    caps_net = capsnet.CapsNet(
            conv_layer=capsnet.ConvLayer(in_channels=3),
            primary_caps_layer=capsnet.PrimaryCapsLayer(
                num_routes=num_routes,
                ),
            final_caps_layer=capsnet.FinalCapsLayer(
                num_routes=num_routes,
                ),
            decoder_network=capsnet.Decoder(
                output_width=32,
                output_height=32,
                output_channels=3,
                ),
            )
    caps_net.to(device)
    optimizer = torch.optim.Adam(caps_net.parameters(), lr=learning_rate)
    for i in range(epochs):
        caps_net.train_epoch(optimizer, train_loader, i, device)
        # caps_net.test_epoch(test_loader, i)
        # if i == 0:
        #     caps_net.primary_caps_layer.tucker_decompose_layers()
        # if i == 1:
        #     caps_net.decoder_network.svd_decompose_layers()
