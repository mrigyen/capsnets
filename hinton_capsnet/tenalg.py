import torch

def fold(unfolded_tensor, mode, shape):
    """
    refolds the mode-`mode` unfolding of a tensor of shape `shape`
    
        In other words, refolds the n-mode unfolded tensor
        into the original tensor of the specified shape.
    
    Parameters
    ----------
    unfolded_tensor : torch.Tensor
        unfolded tensor of shape ``(shape[mode], -1)``
    mode : int
        the mode of the unfolding
    shape : tuple
        shape of the original tensor before unfolding
    
    Returns
    -------
    torch.Tensor
        folded_tensor of shape `shape`
    """
    # TODO what the hell is this
    full_shape = list(shape)
    mode_dim = full_shape.pop(mode)
    full_shape.insert(0, mode_dim)

    return torch.moveaxis(torch.reshape(unfolded_tensor, full_shape), 0, mode)

def unfold(tensor, mode):
    """
    Return a 2D matrix by unfolding `tensor` along the `mode`, starting at 0

    Parameters
    ----------
    tensor : torch.Tensor
    mode : int, default is 0
           indexing starts at 0, therefore mode is in ``range(0, tensor.ndim)``
    
    Returns
    -------
    torch.Tensor
        unfolded_tensor of shape ``(tensor.shape[mode], -1)``
    """
    # TODO understand what is going on here and annotate the code with notes
    return torch.reshape(tensor.moveaxis(mode, 0), (tensor.shape[mode], -1))

def mode_dot(tensor, matrix_or_vector, mode, transpose=False):
    """
    return n-mode product of a tensor and a matrix / vector at the specified mode

    Mathematically: :math:`\\text{tensor} \\times_{\\text{mode}} \\text{matrix or vector}`


    Parameters
    ----------
    tensor : ndarray
        tensor of shape ``(i_1, ..., i_k, ..., i_N)``
    matrix_or_vector : ndarray
        1D or 2D array of shape ``(J, i_k)`` or ``(i_k, )``
        matrix or vectors to which to n-mode multiply the tensor
    mode : int
    transpose : bool, default is False
        If True, the matrix is transposed. 
        For complex tensors, the conjugate transpose is used. 

    Returns
    -------
    ndarray
        `mode`-mode product of `tensor` by `matrix_or_vector`
        * of shape :math:`(i_1, ..., i_{k-1}, J, i_{k+1}, ..., i_N)` if matrix_or_vector is a matrix
        * of shape :math:`(i_1, ..., i_{k-1}, i_{k+1}, ..., i_N)` if matrix_or_vector is a vector

    See also
    --------
    multi_mode_dot : chaining several mode_dot in one call

    based on mode_dot() from tensorly/tenalg/core_tenalg/n_mode_product.py
    """
    # the mode along which to fold might decrease if we take product with a vector
    fold_mode = mode
    new_shape = list(tensor.shape)

    # is matrix
    if matrix_or_vector.ndim == 2:
        if transpose:
            dim = 0
        else:
            dim = 1

        if matrix_or_vector.shape[dim] != tensor.shape[mode]:
            raise ValueError(
                    f"shapes {tensor.shape} and {matrix_or_vector.shape} are not aligned in mode-{mode} multiplication: {tensor.shape[mode]} (mode {mode}) != {matrix_or_vector.shape[dim]}"
                    )
        # replace with conjugate transpose of itself
        if transpose:
            matrix_or_vector = torch.conj(torch.transpose(matrix_or_vector, 0, -1))

        new_shape[mode] = matrix_or_vector.shape[0]
        vec = False

    # is vector
    elif matrix_or_vector.ndim == 1:
        if matrix_or_vector.shape[0] != tensor.shape[mode]:
            raise ValueError(
                    f"shapes {tensor.shape} and {matrix_or_vector.shape} are not aligned in mode-{mode} multiplication: {tensor.shape[mode]} (mode {mode}) != {matrix_or_vector.shape[0]}"
                    )
        if len(new_shape) > 1:
            new_shape.pop(mode)
        else:
            new_shape = []
        vec = True

    else:
        raise ValueError(
                f"Can only take n_mode_product with a vector or a matrix. Provided array of dimension {matrix_or_vector.ndim}, not in [1, 2]"
                )

    # equivalent to tensorly.dot(): see tensorly/backend/pytorch_backend.py:195L
    # also tested via a REPL to see that they are equivalent
    res = torch.matmul(matrix_or_vector, unfold(tensor, mode))

    if vec:
        # return vector
        return torch.reshape(res, new_shape)
    else:
        return fold(res, fold_mode, new_shape)

def multi_mode_dot(tensor, matrix_or_vec_list, modes=None, skip=None, transpose=False):
    """
    n-mode product of a tensor and several matrices or vectors over several modes

    Parameters
    ----------
    tensor : torch.Tensor

    matrix_or_vec_list : list of matrices or vectors of length ``tensor.ndim``

    skip : None or int, optional, default is None
        If not None, index of a matrix to skip. 
        Note that in any case, `modes`, if provided, should have a length of ``tensor.ndim``

    modes : None or int list, optional, default is None

    transpose : bool, optional, default is False
        If True, the matrices or vectors in in the list are transposed.
        For complex tensors, the conjugate transpose is used. 

    Returns
    -------
    torch.Tensor
        tensor times each matrix or vector in the list at mode `mode`

    Notes
    -----
    If no modes are specified, just assumes there is one matrix or vector per mode and returns:

    :math:`\\text{tensor  }\\times_0 \\text{ matrix or vec list[0] }\\times_1 \\cdots \\times_n \\text{ matrix or vec list[n] }`

    See also
    --------
    mode_dot
    """
    if modes is None:
        modes = range(len(matrix_or_vec_list))

    decrement = 0

    res = tensor

    # order of mode dots doesn't matter for different modes
    # sorting by mode shouldn't change order for equal modes
    # get a sorted list of tuples of (matrix_or_vec_list[x], modes[x]), sorted by modes
    factors_modes = sorted(zip(matrix_or_vec_list, modes), key=lambda x: x[1])

    for i, (matrix_or_vec, mode) in enumerate(factors_modes):
        # why even implement this, why would you allow skipping as part of the algorithm
        if (skip is not None) and (i == skip):
            continue

        # TODO why use mode - decrement
        if transpose:
            res = mode_dot(res, torch.conj(torch.transpose(matrix_or_vec, 0, -1)), mode - decrement)
        else:
            res = mode_dot(res, matrix_or_vec, mode - decrement)

        if matrix_or_vec.ndim == 1:
            decrement = decrement + 1

    return res

def norm(tensor, order=None, axis=None):
    """
    Exactly as in tensorly's pytorch backend
    """
    # pytorch does not accept `None` for any keyword arguments. additionally,
    # pytorch doesn't seems to support keyword arguments in the first place
    kwds = {}
    if axis is not None:
        kwds['dim'] = axis
    if order and order != 'inf':
        kwds['p'] = order

    if order == 'inf':
        res = torch.max(torch.abs(tensor), **kwds)
        if axis is not None:
            return res[0]  # ignore indices output
        return res
    return torch.norm(tensor, **kwds)
