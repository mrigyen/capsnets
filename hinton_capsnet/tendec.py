import torch
import tenalg
import math

def partial_svd(
        matrix,
        n_eigenvecs=None,
        ):
    """
    Compute a fast partial SVD on `matrix`

    If `n_eigenvecs` is specified, use sparse eigendecomposition on uh... something
    Based on tensorly's numpy based algorithm, using only torch
    """
    # check matrix is 2D
    assert len(matrix.shape) == 2
    assert type(n_eigenvecs) is int, f"{n_eigenvecs} is not int"
    dim_1, dim_2 = matrix.shape
    min_dim = min(dim_1, dim_2)
    max_dim = max(dim_1, dim_2)

    if n_eigenvecs is None:
        n_eigenvecs = max_dim
    if n_eigenvecs > max_dim:
        # incorrect value; correct it
        n_eigenvecs = max_dim

    full_matrices = n_eigenvecs > min_dim
    U, S, V = torch.linalg.svd(matrix, full_matrices=full_matrices)
    U, S, V = U[:, :n_eigenvecs], S[:n_eigenvecs], V[:n_eigenvecs, :]
    return U, S, V

def partial_tucker(
        tensor,
        modes,
        rank=None,
        n_iter_max=100,
        init="svd",
        tol=10e-5,
        random_state=None,
        verbose=False,
        mask=None):
    """
    Decompose tensor into a Tucker decomposition exclusively along the provided modes
    Based on tensorly's algorithm, and made exclusively to use torch.

    Parameters
    ----------
    tensor: torch.Tensor
    modes: int list
                list of the modes on which to perform the decomposition
    rank: None, int or int list
                size of the core tensor
                if int, use the same rank for all modes
    n_iter_max: int
                maximum number of iterations
    init : {"svd", "random"}, or a tensor
                if a tensor is provided, use for initialization
    tol: float
                the algorithm stops when the variation in the reconstruction error is less than the tolerance
    random_state: {None, int, ???}
                TODO figure out what this is supposed to be used for
    mask: None, or a tensor
                array of booleans with the same shape as ``tensor`` which
                should be 0 where the the values are missing and 1 everywhere
                else.
    """
    if rank is None:
        # use the original size
        rank = [tensor.shape[mode] for mode in modes]
    elif isinstance(rank, int):
        rank = tuple(rank for _ in modes)
    else:
        rank = tuple(rank)

    if mask is not None and init == "svd":
        # TODO handle this
        pass

    # use a partial svd
    if init == "svd":
        factors = []
        for index, mode in enumerate(modes):
            eigenvecs, _, _ = partial_svd(tenalg.unfold(tensor, mode), n_eigenvecs=rank[index])
            factors.append(eigenvecs)

        # core approximation for the masking step
        core = tenalg.multi_mode_dot(tensor, factors, modes=modes, transpose=True)
    elif init == "random":
        core_shape = list(tensor.shape)
        # TODO what the hell is this
        for (i, e) in enumerate(modes):
            core_shape[e] = rank[i]

        # TODO verify this
        core = torch.rand(core_shape)
        factors = [torch.rand((tensor.shape[mode], rank[index])) for (index, mode) in enumerate(modes)]
    else:
        (core, factors) = init

    rec_errors = []
    norm_tensor = tenalg.norm(tensor, 2)

    for iteration in range(n_iter_max):
        if mask is not None:
            tensor = tensor * mask + tenalg.multi_mode_dot(
                    core, factors, modes=modes, transpose=False) * (1 - mask)

        for index, mode in enumerate(modes):
            core_approximation = tenalg.multi_mode_dot(tensor, factors, modes=modes, skip=index, transpose=True)
            eigenvecs, _, _ = partial_svd(tenalg.unfold(core_approximation, mode), n_eigenvecs=rank[index])
            factors[index] = eigenvecs

        core = tenalg.multi_mode_dot(tensor, factors, modes=modes, transpose=True)

        # the factors are orthonormal, and therefore do not affect the reconstructed tensor's norm
        rec_error = math.sqrt(abs(norm_tensor ** 2 - tenalg.norm(core, 2) ** 2)) / norm_tensor
        rec_errors.append(rec_error)

        if iteration > 1:
            if tol and abs(rec_errors[-2] - rec_errors[-1]) < tol:
                break
    return (core, factors)

# TODO create tests
if __name__ == "__main__":
    pass
